const sql = require('sql-template-strings');
const {v4: uuidv4} = require('uuid');
const db = require('./db');

module.exports = {
  async create(name, city, zipcode, address, start_date, end_date) {
    try {

      const {rows} = await db.query(sql`
      INSERT INTO festival (id, name, city, zipcode, address, start_date, end_date)
        VALUES (${uuidv4()}, ${name}, ${city}, ${zipcode}, ${address}, ${start_date}, ${end_date})
        RETURNING name, city, zipcode, address, start_date, end_date;
      `);

      const [festival] = rows;
      return festival;
    } catch (error) {
        throw error;
    }
  },
  async find(id) {
    const {rows} = await db.query(sql`
    SELECT * FROM festival WHERE id=${id} LIMIT 1;
    `);
    return rows[0];
  },

  async delete(id) {
    const {rows} = await db.query(sql`
    DELETE FROM festival WHERE id=${id};
    `);
    return rows[0];
  },

  async findAll() {
    const {rows} = await db.query(sql`
    SELECT * FROM festival;
    `);
    return rows;
  },

  async findFestivalNow() {
    const {rows} = await db.query(sql`
      SELECT * FROM festival f where f.start_date <= now() and f.end_date >= now();
    `);
    return rows;
  }
};
