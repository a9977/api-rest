SELECT 'CREATE DATABASE festival'
    WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'festival')\gexec
;

CREATE TABLE "users"(
    "id" CHAR(255) PRIMARY KEY,
    "email" CHAR(255) NOT NULL,
    "password" CHAR(255) NOT NULL
);

CREATE TABLE "festival"(
    "id" CHAR(255) PRIMARY KEY,
    "name" CHAR(255) NOT NULL,
    "city" CHAR(255) NOT NULL,
    "zipcode" INTEGER NOT NULL,
    "address" CHAR(255) NOT NULL,
    "start_date" DATE NOT NULL,
    "end_date" DATE NOT NULL
);

CREATE TABLE "artists"(
    "id" CHAR(255) PRIMARY KEY,
    "name" CHAR(255) NOT NULL,
    "style" CHAR(255) NOT NULL
);

CREATE TABLE "artists_festival"(
    "id" CHAR(255) PRIMARY KEY,
    "artist_id" CHAR(255) NOT NULL,
    "festival_id" CHAR(255) NOT NULL
);

ALTER TABLE
    "artists_festival" ADD CONSTRAINT "artists_festival_artist_id_foreign" FOREIGN KEY("artist_id") REFERENCES "artists"("id");
ALTER TABLE
    "artists_festival" ADD CONSTRAINT "artists_festival_festival_id_foreign" FOREIGN KEY("festival_id") REFERENCES "festival"("id");