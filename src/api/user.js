const {Router, request, response} = require('express');
const User = require('../persistence/users');

const router = new Router();

router.post('/', async (request, response) => {
  try {
    const {email, password} = request.body;
    if (!email || !password) {
      return response
          .status(400)
          .json({message: 'Vous devez fournir un email & password'});
    }

    const user = await User.create(email, password);
    if (!user) {
      return response.status(400).json({message: "L'user existe déjà"});
    }

    return response.status(200).json(user);
  } catch (error) {
    console.error(
        `createUser({ email: ${request.body.email} }) >> Error: ${error.stack}`
    );
    response.status(500).json();
  }
});

router.get('/', async (request, response) => {
  try {
    const {email} = request.body;
    if (!email) {
      return response
          .status(400)
          .json({message: 'Merci de renseigner un email valide'});
    }

    const user = await User.find(email);

    if (!user) {
      return response.status(400).json({message: "L'utilisateur n'existe pas"});
    }
    return response.status(200).json(user);
  } catch (error) {
    console.error(
        `getUserByID({ email: ${request.body.email} }) >> Error: ${error.stack}`
    );
    response.status(500).json();
  }
})

router.get('/all', async (request, response) => {
  try {
    const users = await User.findAll();

    if (!users) {
      return response.status(400).json({message: "Aucun utilisateur"});
    }
    return response.status(200).json(users);
  } catch (error) {
    console.error(
        `getAllUsers(Error: ${error.stack}`
    );
    response.status(500).json();
  }
});


router.delete('/delete', async (request, response) => {
  try {
    const {id} = request.body;
    if (!id) {
      return response
          .status(400)
          .json({message: 'Merci de renseigner un id valide'});
    }

    const user = await User.find(id);

    if (!user) {
      return response.status(400).json({message: "L'utilisateur n'existe pas"});
    }

    await User.delete(user);

    return response.status(200).json(user);
  } catch (error) {
    console.error(
        `deleteUser({ email: ${request.body.id} }) >> Error: ${error.stack}`
    );
    response.status(500).json();
  }
});

module.exports = router;
