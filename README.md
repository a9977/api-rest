# REST API

> API utilisée dans le cadre du projet App Festival

## Installation

Pre-requisites:

- Docker

Run `docker-compose up` in the root of the project.

It will bring up Postgres and the Express application server in development mode.

It binds the application server to `localhost:3000`.

## Database setup + management

`npm run migrate up` will run the migrations.

`npm run migrate down` will roll back the migrations.

`npm run migrate:create <migration-name>`  will create a new migration file in [./src/migrations](./src/migrations).

To run the migrations inside of docker-compose. Which will run a bash instance inside the `app` container.

```sh
docker-compose run app bash
```

Followed by:
```sh
npm run migrate up
```

Pour se connecter à Adminer et administrer notre base de données :

```sh
url : localhost:8080
```

![img.png](img.png)

mdp : postgres

