const {Router} = require('express');
const Artist = require('../persistence/artist')
const router = new Router();

router.post('/', async (request, response) => {
  try {
    const {name, style} = request.body;
    if (!name || !style) {
      return response
        .status(400)
        .json({message: 'Vous devez fournir un nom & style'});
    }

    const artist = await Artist.create(name, style);
    if (!artist) {
      return response.status(400).json({message: "L'artiste existe déjà"});
    }

    return response.status(200).json(artist);
  } catch (error) {
    console.error(
      `createArtist({ email: ${request.body.firstname} }) >> Error: ${error.stack}`
    );
    response.status(500).json();
  }
});

router.get('/', async (request, response) => {
    try {
        const {id} = request.body;
        if (!id) {
            return response
                .status(400)
                .json({message: 'Merci de renseigner un id valide'});
        }

        const artist = await Artist.find(id);

        if (!artist) {
            return response.status(400).json({message: "L'artiste n'existe pas"});
        }
        return response.status(200).json(artist);
    } catch (error) {
        console.error(
            `getArtistByID({ email: ${request.body.id} }) >> Error: ${error.stack}`
        );
        response.status(500).json();
    }
})

router.get('/all', async (request, response) => {
    try {
        const artists = await Artist.findAll();

        if (!artists) {
            return response.status(400).json({message: "Aucun artiste"});
        }
        return response.status(200).json(artists);
    } catch (error) {
        console.error(
            `getAllArtists(Error: ${error.stack}`
        );
        response.status(500).json();
    }
});


router.delete('/delete', async (request, response) => {
    try {
        const {id} = request.body;
        if (!id) {
            return response
                .status(400)
                .json({message: 'Merci de renseigner un id valide'});
        }

        const artist = await Artist.find(id);

        if (!artist) {
            return response.status(400).json({message: "L'artiste n'existe pas"});
        }

       await Artist.delete(artist);

        return response.status(200).json(artist);
    } catch (error) {
        console.error(
            `deleteArtist({ email: ${request.body.id} }) >> Error: ${error.stack}`
        );
        response.status(500).json();
    }
});


module.exports = router;
