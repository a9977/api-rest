const express = require('express');

const {Router} = express;
const router = new Router();

const user = require('./user');
const artist = require('./artist');
const festival = require('./festival');

router.use('/api/users', user);
router.use('/api/artists', artist);
router.use('/api/festival', festival);

module.exports = router;
