const {Router} = require('express');
const Festival = require("../persistence/festival");

const router = new Router();


router.post('/', async (request, response) => {
    try {
        const {name, city, zipcode, address, start_date, end_date} = request.body;
        if (!name || !city || !zipcode ||!address ||!start_date ||!end_date) {
            return response
                .status(400)
                .json({message: 'Vous devez fournir un nom, ville, code postal, adresse, date_debut, date_fin', name, city, zipcode, address, start_date, end_date});
        }

        const festival = await Festival.create(name, city, zipcode, address, start_date, end_date);
        if (!festival) {
            return response.status(400).json({message: "Le festival existe déjà"});
        }

        return response.status(200).json(festival);
    } catch (error) {
        console.error(
            `createFestival({ name: ${request.body.name}, city: ${request.body.city}, zipcode: ${request.body.zipcode}, address: ${request.body.address}, start_date: ${request.body.start_date}, end_date: ${request.body.end_date},  }) >> Error: ${error.stack}`
        );
        response.status(500).json();
    }
});

router.get('/', async (request, response) => {
    try {
        const {id} = request.body;
        if (!id) {
            return response
                .status(400)
                .json({message: 'Merci de renseigner un id valide'});
        }

        const festival = await Festival.find(id);

        if (!festival) {
            return response.status(400).json({message: "Le festival n'existe pas"});
        }
        return response.status(200).json(festival);
    } catch (error) {
        console.error(
            `getFestivalByID({ email: ${request.body.id} }) >> Error: ${error.stack}`
        );
        response.status(500).json();
    }
})

router.get('/all', async (request, response) => {
    try {
        const festivals = await Festival.findAll();

        if (!festivals) {
            return response.status(400).json({message: "Aucun festival"});
        }
        return response.status(200).json(festivals);
    } catch (error) {
        console.error(
            `getAllFestivals(Error: ${error.stack}`
        );
        response.status(500).json();
    }
});

router.get('/now', async (request, response) => {
    try {
        const festivals = await Festival.findFestivalNow();

        if (!festivals) {
            return response.status(400).json({message: "Aucun festival en ce moment"});
        }
        return response.status(200).json(festivals);
    } catch (error) {
        console.error(
            `getAllFestivals(Error: ${error.stack}`
        );
        response.status(500).json();
    }
});

router.delete('/delete', async (request, response) => {
    try {
        const {id} = request.body;
        if (!id) {
            return response
                .status(400)
                .json({message: 'Merci de renseigner un id valide'});
        }

        const festival = await Festival.find(id);

        if (!festival) {
            return response.status(400).json({message: "Le festival n'existe pas"});
        }

        await Festival.delete(id);

        return response.status(200).json(festival);
    } catch (error) {
        console.error(
            `deleteFestival({ email: ${request.body.id} }) >> Error: ${error.stack}`
        );
        response.status(500).json();
    }
});


module.exports = router;
