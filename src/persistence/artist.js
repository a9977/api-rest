const sql = require('sql-template-strings');
const {v4: uuidv4} = require('uuid');
const bcrypt = require('bcrypt');
const db = require('./db');

module.exports = {
  async create(name, style) {
    try {

      const {rows} = await db.query(sql`
      INSERT INTO artists (id, name, style)
        VALUES (${uuidv4()}, ${name}, ${style})
        RETURNING id, name, style;
      `);

      const [artists] = rows;
      return artists;
    } catch (error) {
      throw error;
    }
  },
  async find(id) {
    const {rows} = await db.query(sql`
    SELECT * FROM artists WHERE id=${id};
    `);
    return rows[0];
  },

  async delete(id) {
    const {rows} = await db.query(sql`
    DELETE FROM artists WHERE id=${id};
    `);
    return rows[0];
  },

  async findAll() {
    const {rows} = await db.query(sql`
    SELECT * FROM artists;
    `);
    return rows;
  }
};
